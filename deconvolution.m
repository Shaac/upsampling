function H = deconvolution (Htilde, ff, fdx, fdy, lambda1, lambda2, n)
    % Implement the decovolution process as detailed in the supplementary file.
    fHtilde = fft2(Htilde); % Will be used in H step.
    fHstar  = fHtilde;      % Initialisation.

    for i = 1 : n
        [mux, muy] = mustep(fHstar, lambda1, lambda2, fdx, fdy);
        fHstar = Hstep(ff, fHtilde, fdx, fdy, mux, muy, lambda2);
        lambda2 *= 3;
    end

    H = uint8(real(ifft2(fHstar)));
endfunction

function [mux, muy] = mustep (fH, lambda1, lambda2, fdx, fdy)
    % Compute the μ matrix by solving the μ step equation: Eq. (4).
    % As Φ we use absolute value.
    dxH = real(ifft2(fdx .* fH));
    dyH = real(ifft2(fdy .* fH));
    const = lambda1 / (2 * lambda2);
    mux = (dxH > const) .* (dxH - const) + (dxH < - const) .* (dxH + const);
    muy = (dyH > const) .* (dyH - const) + (dyH < - const) .* (dyH + const);
endfunction

function fHstar = Hstep(ff, fHtilde, fdx, fdy, mux, muy, lambda2)
    % Compute the fourier transform of H* according to the formula (a big
    % fraction) in the “H step” part of the article.
    fHstar = (conj(ff) .* fHtilde + ...
        lambda2 * conj(fdx) .* fft2(mux) + ...
        lambda2 * conj(fdy) .* fft2(muy)) ./ ...
        (conj(ff) .* ff + ...
        lambda2 * conj(fdx) .* fdx + ...
        lambda2 * conj(fdy) .* fdy);
endfunction
