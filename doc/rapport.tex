\documentclass{article}

\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{color}
\usepackage{amsmath}

\lstset{language=Octave,
basicstyle=\scriptsize,
keywordstyle=\color{red}\bfseries,
commentstyle=\color{blue}\textit,
stringstyle=\color{green}\ttfamily,
showspaces=false,
showstringspaces=false}

\begin{document}

\title{Zoom rapide — Projet de SI241}
\author{Sacha Delanoue}
\date{20 mai 2014}

\maketitle

\tableofcontents

\newpage

\section{Présentation du projet}

\subsection{L’articles scientifique}

Ce projet est l’implémentation d’un article scintifique intitulé
\emph{Fast Image/Video Upsampling}, paru en décembre 2008 et écrit par
Qi Shan, Zhaorong Li, Jiaya Jia et Chi-Keung Tang.

Cet article présente une méthode de sur-échantillonnage (c’est-à-dire de zoom)
pour des images réelles. Il part de la supposition que l’image de départ est la
convolution de l’image réelle par une fonction naturelle, et décimée; il
propose alors de retrouver les pixels décimés en effectuant une déconvolution.

Cet algorithme est présenté comme étant rapide, ne nécessitant que l’image
elle-même pour retrouver les informations perdue, et fonctionnant aussi pour
les vidéos.

\subsection{L’algorithme}

Voici un schéma tiré de l’article qui présente les différentes étapes de
l’algorithme:

\begin{center}
    \includegraphics[height=3cm]{figures/steps.png}
\end{center}

\begin{itemize}
    \item L’étape en rose est la première étape de l’algorithme, consistant en
        un zoom classique, avec une méthode cubique;
    \item l’étape en verte est l’étape cruciale de l’algorithme, consistant en
        la déconvolution de l’image;
    \item l’étape en grise est l’étape de contrôle de l’algrithme, consistant
        en la reconvolution de l’image, puis en la substitution des pixels
        connus par leur valeur dans l’image originale.
\end{itemize}

À la fin de chaque déconvolution, une image de meilleure qualité est obtenue,
l’algorithme s’effectue donc en un nombre d’itérations données.

\section{Implémentation}

J’ai implémenté l’algorithme présenté dans l’article à l’aide du lagage et du
logiciel libre Octave. Cette section décrit l’implémentation.

\subsection{Prétraitement}

L’algorithme traite l’image à l’aide de transformée de Fourier. On s’attend
donc à ce qu’il y ai un problème avec les bords. Pour éviter ce genre de
problèmes, on symétrise l’image avant de lui appliquer l’algorithme. Ainsi, les
bords de l’images sont alors continus.

Ceci est très facile à implémenter avec Octave:
\begin{lstlisting}
function ret = mirrors (im)
    ret = [im, fliplr(im)];
    ret = [ret; flipud(ret)];
endfunction
\end{lstlisting}

\subsection{Zoom cubique}

La première étape de l’algorithme est d’obtenir une première version de l’image
sur-échantillonnée à l’aide d’un algorithme classique.

L’article suggère d’utiliser une méthode cubique, mais ne donne pas plus de
détails pour son implémentation. Il existe de nombreuses méthodes
d’interpolation cubique.

La méthode utilisée ici est celle qui m’a été implémentée en dimension 1 par M.
Saïd Ladjal: il s’agit d’une convolution par une $\beta$-spline.

\subsubsection{Dimension 1}

Je me suis approprié le code qui m’a été fourni, et l’ai réorganisé de manière
à ce que je saississe mieux ce qu’il fait:

\begin{lstlisting}
function ret = cubic (x, f, bspline)
    % Compute the fourier Transform.
    fourier = fft(x) ./ fft([2/3, 1/6 zeros(1, length(x) - 3) 1/6]);

    % Create the bigger array, with lots of zeros.
    big = zeros(1, f * length(x));
    big(1 : f : end) = real(ifft(fourier));

    % Do the convolution with the B-spline.
    shifted = real(ifft(fft(big) .* fft(bspline, length(big))));

    % Shift the result correctly.
    ret = [shifted(f * 2 + 1 : end) shifted(1 : f * 2)];
endfunction
\end{lstlisting}

\subsubsection{Dimension 2}

L’image étant de dimension 2, il faut adapter le code ci-dessus pour qu’il
fonctionne en dimension 2. Pour ce faire, on applique la dimension 1 sur chaque
ligne et chaque colonne.

\begin{lstlisting}
function result = bcupsampling (img, fact)
    % Cubic B-spline expression for |t| <= 2.
    t = (-2 * fact : 2 * fact) / fact;
    bspline = (2/3 - t .^ 2 + abs(t) .^ 3 / 2) .* (abs(t) < 1) ...
        + (2 - abs(t)) .^ 3 / 6 .* (abs(t) >= 1);

    % Apply "cubic" on every line and every column.
    tmp = [];
    result = [];
    i = 1;
    for line = img
        tmp(i, :) = cubic(line', fact, bspline);
        i ++;
    end
    i = 1;
    for line = tmp
        result(i, :) = cubic(line', fact, bspline);
        i ++;
    end
endfunction
\end{lstlisting}

Cette fonction peut être testée, et on peut alors s’assurer qu’elle fonctionne
bien.

\subsection{Déconvolution}

La déconvolution est le cœur de l’algorithme. Un supplément à l’article lui est
consacrée afin d’expliquer en détail comment elle fonctionne.

\subsubsection{Équation}

À la $k$\ieme{} étape, on a une image $\widetilde H^{(k)}$ que l’on veut
déconvoluer en une image $H^{(k)}$ (telle que $f \otimes H^{(k)} = \widetilde
H^{(k)}$). L’article se base sur une méthode similaire à celle proposée dans
une autre recherche, en introduisant une fonction $\Phi$, et en réduisant la
recherche de $H$ à la solution qui minimise:

\[E(H) \propto \|f\otimes H - \widetilde H\|_2^2 +
\lambda_1(\|\Phi(\partial_xH)\|_1 + \|\Phi(\partial_yH)\|_1)\]

On prendra $\Phi(x) = |x|$ pour notre implémentation, l’article lui utilise une
fonction plus compliquée définie par morceaux.

\subsubsection{Méthode}

Pour la résolution de cette équation, l’article introduit une variable
$\mu = (\mu_x, \mu_y)$ pour substituer
$\partial H = (\partial_xH, \partial_yH)$. Il indroduit également un terme
$\lambda_2$ pour mesurer la différence entre $\partial H$ et $\mu$.

L’équation devient alors:
\[E(H, \mu) = \|f\otimes H - \widetilde H\|_2^2 +
\lambda_1(\|\Phi(\mu_x)\|_1 + \|\Phi(\mu_yH)\|_1) +
\lambda_2(\|\mu_x - \partial H\|_2^2 + \|\mu_y - \partial
H\|_2^2)\]

L’intérêt d’introduire $\mu$ est que les équations avec $H$ ou $\mu$ fixée sont
faciles à résoudre.

\subsubsection{Étape $\mu$}

Dans cette étape on fixe $H$ et on résout l’équation en fonction de $\mu$.

Non seulement $\mu_x$ et $\mu_y$ sont indépendants l’un de l’autre dans cette
équation, mais aussi chaque \og case\fg{} de ces matrices: $E(\mu) = \sum_{x,y}
E(\mu_x(x,y)) + E(\mu_y(x,y))$.

Ainsi, $E(\mu_x(x,y)) = \lambda_1|\mu_x(x,y)| + \lambda_2(\mu_x(x,y) -
\partial_xH)^2$ (de même pour $E(\mu_y(x,y))$).
Minimiser cette équation est simple, la solution est $\mu_x \in \{
    \partial_xH - \frac{\lambda_1}{2\lambda_2}, 0,
\partial_xH + \frac{\lambda_1}{2\lambda_2} \}$ selon qu’il soit positif, nul,
ou négatif. De même pour $\mu_y$.

L’implémentation de l’étape $\mu$ est donc la suivante:

        \begin{lstlisting}
function [mux, muy] = mustep (fH, lambda1, lambda2, fdx, fdy)
    dxH = real(ifft2(fdx .* fH));
    dyH = real(ifft2(fdy .* fH));
    const = lambda1 / (2 * lambda2);
    mux = (dxH > const) .* (dxH - const) + (dxH < - const) .* (dxH + const);
    muy = (dyH > const) .* (dyH - const) + (dyH < - const) .* (dyH + const);
endfunction
        \end{lstlisting}

\subsubsection{Étape $H$}

Pour cette étape on fixe $\mu$, et on minimise l’équation. L’article donne
directement la solution en Fourier:
\[\mathcal F^*(H) = \dfrac
    {\overline{\mathcal F(f)} \circ \mathcal F(\widetilde H) +
        \lambda_2\overline{\mathcal F(\partial_x)} \circ\mathcal F(\mu_x) +
    \lambda_2\overline{\mathcal F(\partial_y)}\circ\mathcal F(\mu_y)}
    {\overline{\mathcal F(f)} \circ \mathcal F(f) +
        \lambda_2\overline{\mathcal F(\partial_x)}\circ\mathcal F(\partial_x) +
\lambda_2\overline{\mathcal F(\partial_y)}\circ\mathcal F(\partial_y)}\]

Puisque ce n’est que du Fourier l’implémentation est relativement simple:

\begin{lstlisting}
function fHstar = Hstep(ff, fHtilde, fdx, fdy, mux, muy, lambda2)
    fHstar = (conj(ff) .* fHtilde + ...
        lambda2 * conj(fdx) .* fft2(mux) + ...
        lambda2 * conj(fdy) .* fft2(muy)) ./ ...
        (conj(ff) .* ff + ...
        lambda2 * conj(fdx) .* fdx + ...
        lambda2 * conj(fdy) .* fdy);
endfunction
\end{lstlisting}

\subsubsection{Implémentation}

L’implémentation de la déconvolution fait donc appel aux fonctions des deux
étapes. À la suite de chaque appel à ces deux fonctions, $\lambda_2$ triplé
pour assurer la convergence. Le nombre d’itérations est décidé à l’avance;
l’article suggère d’en faire 4.

Voici la fonction de déconvolution appelant les deux précédentes:
\begin{lstlisting}
function H = deconvolution (Htilde, ff, fdx, fdy, lambda1, lambda2, n)
    fHtilde = fft2(Htilde); % Will be used in H step.
    fHstar  = fHtilde;      % Initialisation.

    for i = 1 : n
        [mux, muy] = mustep(fHstar, lambda1, lambda2, fdx, fdy);
        fHstar = Hstep(ff, fHtilde, fdx, fdy, mux, muy, lambda2);
        lambda2 *= 3;
    end

    H = uint8(real(ifft2(fHstar)));
endfunction
\end{lstlisting}

\subsection{Boucle de contrôle}

Une simple déconvolution est sensée donner un résultat satisfaisant mais il
reste encore améliorable. De plus, la déconvolution n’est pas quelque chose de
nouveau apporté par cet article. La spécificité de l’article, et ce qui permet
par ailleurs à l’algorithme de fonctionner pour des vidéos, c’est la boucle de
contrôle consistant en une reconvolution de l’image pour ensuite substituer
les pixels connus par ceux de l’image originale. L’image obtenue est alors
prête pour une nouvelle déconvolution.

\subsubsection{Reconvolution}

La reconvolution est une étape très simple, il suffit juste de convoluer
l’image obtenue à l’étape précédente par la fonction de flou naturel:
\begin{lstlisting}
Hs = uint8(real(ifft2(fft2(H) .* ff)));
\end{lstlisting}

Parlons de cette fonction de flou naturel. L’article conseille d’utiliser un
flou Gaussien de taille $13\times13$. La variance utilisée par l’article a été
obtenue expérimentalement, et des valeurs sont données pour des zooms de
facteur 2, 4 et 8. Afin d’avoir un programme le plus générique possible, j’ai
utilisé un polynôme du second degré passant par ces trois valeur pour qu’un
autre facteur de zoom puisse être donné.

Voici mon implémentation du noyau Gaussian:
\begin{lstlisting}
function ret = gaussianKernel(kerSize, sigma, l, c)
    h      = uint32((kerSize - 1) / 2);
    [X, Y] = meshgrid(0 : h, 0 : h);
    ker    = exp(-(X .^ 2 + Y .^ 2) / (2 * sigma ^ 2));
    ker    = ker / sum(sum(ker));

    ret = [ker, zeros(h + 1, c - (2 * h) - 1), fliplr(ker(:, 2 : h + 1)); ...
        zeros(l - (2 * h) - 1, c);
        flipud(ker(2:h+1,:)), zeros(h, c - 2 * h - 1), ...
            flipud(fliplr(ker(2:h+1,2:h+1)))];
endfunction
\end{lstlisting}

Il est implémenté de façon à ce qu’il soit centré sur la case en haut à gauche,
afin que les convolutions ne décalent pas l’image.

\subsubsection{Substitution des pixels}

Dans l’image reconvolué, on remplace les pixels dont on connait la valeur
(c’est le cas d’un pixel sur $f^2$, où $f$ est le facteur de zoom) par ceux
de l’image originale.

En Octave cela s’implémente ainsi:
\begin{lstlisting}
Htilde = Hs;
Htilde(1:factor:end, 1:factor:end) = original;
\end{lstlisting}

\subsubsection{Implémentation}

L’implémentation de la boucle correspond à l’implémentation de l’algorithme
en entier. La voici:

\begin{lstlisting}
function H = main (L)
    factor = 4;
    lambda1 = 0.04;
    lambda2 = 20;
    n = 4;

    % Intialize the image: add symetry, and apply bicubic upsampling.
    original = mirrors(L);
    Htilde = uint8(bcupsampling(original, factor));

    % Initialize some Fourier transforms to be used later.
    [l, c] = size(Htilde);
    fdx = fft2([1, -1, zeros(1, c - 2); zeros(l - 1, c)]);
    fdy = fft2([1, zeros(1, c - 1); -1, zeros(1, c - 1); zeros(l - 2, c)]);

    % For the gaussian kernel, the deviation is computed with a polynom giving
    % the article experimental values when the factor is 2, 4 or 8.
    deviation = - 19 / 1200 * factor ^ 2 + 8 / 25 * factor + 71 / 150;
    ff = fft2(gaussianKernel(13, deviation, l, c));

    for step = 1 : 4
        % Do the deconvolution.
        H = deconvolution(Htilde, ff, fdx, fdy, lambda1, lambda2, n);

        % Do the reconvolution.
        Hs = uint8(real(ifft2(fft2(H) .* ff)));

        % Do the pixel substitution.
        Htilde = Hs;
        Htilde(1:factor:end, 1:factor:end) = original;
    end

    H = H(1 : end / 2, 1 : end / 2);
endfunction
\end{lstlisting}

\section{Résultats}

\subsection{Résultats de l’implémentation}

En théorie, tout ce qui est dit dans l’article est implémenté, et donc
tout devrait fonctionner. En pratique, cela ne fonctionne pas.

Dès la première itération (c’est-à-dire après la première déconvolution),
l’image est assombrie. Elle n’est pourtant pas assombrie à la suite des
itérations suivantes. De plus, alors que chaque itération devrait améliorer
l’image, elle ne fait que la déteriorer.

Voici un exemple avec une image de Lena. Ceci est l’image obtenue après
l’interpolation bicubique:\\
\includegraphics[width=\textwidth]{figures/out0.png}\\
Voici l’image après la première itération:\\
\includegraphics[width=\textwidth]{figures/out1.png}\\
Et la voici après la cinquième:\\
\includegraphics[width=\textwidth]{figures/out5.png}\\

\subsection{Analyse}

\subsubsection{Analyse de l’implémentation}

Pourquoi donc cet échec? J’ai eu le temps d’essayer de trouver des pistes pour
faire fonctionner cet algorithme. J’étais convaincu que j’étais très proche de
la solution et qu’il ne s’agissait que d’une simple erreur quelque part dans
l’implémentation mais je n’ai pas réussi à trouver.

La première possibilité qui vient en tête est que l’algorithme décrit dans
l’article ne fonctionne pas vraiment. Après tout il parait bien trop beau.
Mais pourtant le programme fourni par les chercheurs fonctionne bien sur cette
même image, c’est donc que cet algorithme doit bien fonctionner.

Est-ce une erreur mathématique dans l’implémentation? Après tout il y a
beaucoup de formules mathématiques et j’ai pu me tromper dans leur passage au
numérique. Mais après de nombreuses relectures je suis relativement confiant
que cela ne vient pas de là.

Une autre possibilité qui m’est venu en tête est la possibité d’un overflow de
la part d’Octave. La dernière version de Matlab gère les nombres différemment
et cause plein de soucis avec les nombres. Peut-être est-ce similaire avec
Octave. Je m’étais dis qu’un overflow pouvait expliquer pourquoi l’image était
assombrie à la première itération. J’ai exploré cette piste mais sans succès.

Je ne sais pas pourquoi l’implémentation ne fonctionne pas, sinon j’aurais
trouver un moyen de la faire fonctionner. J’ai essayé de désassembler le code
de l’implémentation des chercheurs mais je n’ai obtenu que de l’assembleur pur.

\subsubsection{Analyse de l’algorithme}

Je n’ai pas une implémentation qui fonctionne donc mais je peux faire quelques
remarques sur les résultats de l’implémentation faite par les chercheurs.

Le zoom obtenu est de très bonne qualité il est vrai, mais j’ai du mal à
comprendre en quoi il est rapide. Il faut 2 minutes pour zoomer une petite
image en noir et blanc avec le processeur. Peut-être qu’avec la carte graphique
cela est beaucoup plus rapide (je n’ai pas de quoi tester) mais cela n’augure
pas de très bonnes performances.
\end{document}
