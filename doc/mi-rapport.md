Sacha Delanoue

Algorithme de zoom rapide — rapport intermédiare
================================================

L’article
---------

L’article présente un algorithme pour agrandir une image, c’est-à-dire
de la sur-échantillonner. Il part de la supposition que l’image de départ
est la convolution de l’image réelle par une fonction naturelle, et décimée,
pour proposer de retrouver les pixels décimées en faisant une déconvolution.

Voici un résumé des étapes de l’algorithme, qui sont donc les étapes de
l’implémentation :

- agrandir l’image avec une méthode cubique ;
- déconvoluer l’image ;
- reconvoluer l’image ;
- substituer les pixels connus de l’image de départ dans l’image résultat ;
- recommencer la déconvolution et ce qui suit jusqu’à convergence.

L’implémentation
----------------

### Pré-traitement

Puisque l’image va être traitée en Fourier, il faut faire quelque chose pour
les bords. Elle sera donc symétrisée 4 fois (dans toutes les directions).
Ceci a été implémenté facilement en deux simples lignes avec Octave.

### Zoom

Il faut un premier zoom avant de pouvoir commencer la boucle de déconvolution
avec feedback. Afin d’avoir un bon résultat un filtre bicubique est utilisé.
La version qui m’a été proposée via un code Matlab en dimension 1 par M. Saïd
Ladjal utilise une B-spline (interpolation cubique d’arguments 1 et 0). Après
m’être approprié ce code, le passage en dimension 2 consistait simplement à
l’appliquer à toutes les lignes et toutes les colonnes.

### Déconvolution

Pour le moment aucune ligne de code n’a été écrite sur cette section – qui est
la partie diffile de l’implémentation. La difficulté sur cette section est
principalement mathématique ce qui n’est pas mon fort. Il ne me manque pas
grand chose pour pouvoir implémenter, et j’ai isolé précisemmment les points
qui me faisaient défaut, mais pour l’instant je n’ai qu’une
compréhension et une résolution mathématique incomplète du problème.

Ce qu’il reste à implémenter
----------------------------

### Déconvolution

La déconvolution donc, qui sera faisable une fois que j’aurais saisi les
quelques points encore flous.

### Reconvolution

La reconvolution est une étape qui parait simple, je ne visualise pas encore
avec une précision totale comment elle sera faite, mais je ne m’attends pas à
une grande difficulté.

### Substitution

La substitution des pixels originaux prendra une ligne en Octave.

### Assemblage des briques

Il ne restera alors plus qu’à assembler les briques dans une unique fonction,
et faire la boucle, avec la condition d’arrêt, ce qui ne présente aucune
difficulté technique.

Comparaisons avec le résultat attendu
-------------------------------------

Je pourrais alors tester mon programme sur les images de l’article et comparer
avec ce qui était attendu, ainsi que de comparer avec le programme fourni avec
l’article.
