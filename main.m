function H = main (L)
    factor = 4;
    lambda1 = 0.04;
    lambda2 = 20;
    n = 4;

    % Intialize the image: add symetry, and apply bicubic upsampling.
    original = mirrors(L);
    Htilde = uint8(bcupsampling(original, factor));

    % Initialize some Fourier transforms to be used later.
    [l, c] = size(Htilde);
    fdx = fft2([1, -1, zeros(1, c - 2); zeros(l - 1, c)]);
    fdy = fft2([1, zeros(1, c - 1); -1, zeros(1, c - 1); zeros(l - 2, c)]);

    % For the gaussian kernel, the deviation is computed with a polynom giving
    % the article experimental values when the factor is 2, 4 or 8.
    deviation = - 19 / 1200 * factor ^ 2 + 8 / 25 * factor + 71 / 150;
    ff = fft2(gaussianKernel(13, deviation, l, c));

    imwrite(Htilde(1 : end / 2, 1 : end / 2), 'out0.tif');
    for step = 1 : 4
        % Do the deconvolution.
        H = deconvolution(Htilde, ff, fdx, fdy, lambda1, lambda2, n);

        % Do the reconvolution.
        Hs = uint8(real(ifft2(fft2(H) .* ff)));

        % Do the pixel substitution.
        Htilde = Hs;
        Htilde(1:factor:end, 1:factor:end) = original;

        imwrite(H(1 : end / 2, 1 : end / 2), sprintf('out%d.tif', step));
    end

    H = H(1 : end / 2, 1 : end / 2);
endfunction

function ret = mirrors (im)
    ret = [im, fliplr(im)];
    ret = [ret; flipud(ret)];
endfunction

function ret = gaussianKernel(kerSize, sigma, l, c)
    h      = uint32((kerSize - 1) / 2);
    [X, Y] = meshgrid(0 : h, 0 : h);
    ker    = exp(-(X .^ 2 + Y .^ 2) / (2 * sigma ^ 2));

    ret = [ker, zeros(h + 1, c - (2 * h) - 1), fliplr(ker(:, 2 : h + 1)); ...
           zeros(l - (2 * h) - 1, c);
           flipud(ker(2:h+1,:)), zeros(h, c - 2 * h - 1), ...
               flipud(fliplr(ker(2:h+1,2:h+1)))];
    ret    = ret / sum(sum(ret));
endfunction
