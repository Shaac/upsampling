function result = bcupsampling (img, fact)
    % Cubic β-spline expression for |t| ≤ 2.
    t = (-2 * fact : 2 * fact) / fact;
    bspline = (2/3 - t .^ 2 + abs(t) .^ 3 / 2) .* (abs(t) < 1) ...
        + (2 - abs(t)) .^ 3 / 6 .* (abs(t) >= 1);

    % Apply “cubic” on every line and every column.
    tmp = [];
    result = [];
    i = 1;
    for line = img
        tmp(i, :) = cubic(line', fact, bspline);
        i ++;
    end
    i = 1;
    for line = tmp
        result(i, :) = cubic(line', fact, bspline);
        i ++;
    end
endfunction

function ret = cubic (x, f, bspline)
    % Compute the fourier Transform.
    fourier = fft(x) ./ fft([2/3, 1/6 zeros(1, length(x) - 3) 1/6]);

    % Create the bigger array, with lots of zeros.
    big = zeros(1, f * length(x));
    big(1 : f : end) = real(ifft(fourier));

    % Do the convolution with the β-spline.
    shifted = real(ifft(fft(big) .* fft(bspline, length(big))));

    % Shift the result correctly.
    ret = [shifted(f * 2 + 1 : end) shifted(1 : f * 2)];
endfunction
